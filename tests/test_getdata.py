"""Test the getdata module."""

import platform
from typing import Callable, Generator, NamedTuple

from pytest_mock.plugin import MockerFixture

from redata import getdata


class svmem(NamedTuple):  # noqa: N801
  """Named tuple for the svmem mock."""

  total: int
  available: int
  percent: float
  used: int
  free: int
  active: int
  inactive: int
  buffers: int
  cached: int
  shared: int
  slab: int


class sswap(NamedTuple):  # noqa: N801
  """Named tuple for the sswap mock."""

  total: int
  used: int
  free: int
  percent: float
  sin: int
  sout: int


class sdiskusage(NamedTuple):  # noqa: N801
  """Named tuple for the sdiskusage mock."""

  total: int
  used: int
  free: int
  percent: float


def test_cpu_usage(mocker: Callable[..., Generator[MockerFixture, None, None]]) -> None:
  """Test the get_cpu_usage function."""
  mocker.patch("redata.getdata.psutil.cpu_percent", return_value=0.0)
  assert getdata.get_cpu_usage() == "CPU utilisé : 0.0%"


def test_memory_usage(
  mocker: Callable[..., Generator[MockerFixture, None, None]],
) -> None:
  """Test the get_memory_usage function."""
  mocker.patch(
    "redata.getdata.psutil.virtual_memory",
    return_value=svmem(
      total=50409836544,
      available=35848105984,
      percent=28.9,
      used=13582921728,
      free=26413703168,
      active=3927195648,
      inactive=18468433920,
      buffers=4743168,
      cached=10408468480,
      shared=370655232,
      slab=685223936,
    ),
  )
  assert getdata.get_ram_usage() == "Mémoire utilisée : 12.65 Go"


def test_no_swap_usage(
  mocker: Callable[..., Generator[MockerFixture, None, None]],
) -> None:
  """Test the get_swap_usage function."""
  mocker.patch(
    "redata.getdata.psutil.swap_memory",
    return_value=sswap(
      total=17179865088, used=0, free=17179865088, percent=0.0, sin=0, sout=0
    ),
  )
  assert getdata.get_swap_usage() == "Swap utilisé : 0.0 Go"


def test_free_disk_space(
  mocker: Callable[..., Generator[MockerFixture, None, None]],
) -> None:
  """Test the get_free_disk_space function."""
  mocker.patch(
    "redata.getdata.psutil.disk_usage",
    return_value=sdiskusage(
      total=361817772032, used=92652236800, free=267855220736, percent=25.7
    ),
  )
  assert (
    getdata.get_free_disk_space()
    == "Espace disque libre au point de montage /: 249.46 GB"
  )


def test_no_gpu(
  mocker: Callable[..., Generator[MockerFixture, None, None]],
) -> None:
  """Test the get_gpu_usage function."""
  mocker.patch("redata.getdata.GPUtil.getGPUs", return_value=[])
  assert getdata.get_gpu_usage() == []
