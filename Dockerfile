FROM alpine:3.19
ARG VERSION=1.0

RUN apk update && apk upgrade && apk add --no-cache python3 poetry py3-pip openssh-client-default

COPY pyproject.toml /app/
COPY redata /app/

WORKDIR /app
CMD poetry install --no-root --no-cache --no-interaction && poetry cache clear pypi --all
