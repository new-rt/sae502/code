"""Parse the json file."""
import json

json_path: str = "./config.json"


def get_config_dict() -> dict:
  """Get the config dict from the json file.

  Returns:
      dict: Config dict
  """
  with open(json_path, "r") as fichier:
    read = fichier.read()
  return json.loads(read)
