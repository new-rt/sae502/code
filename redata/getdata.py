"""Module pour récupérer les données de l'ordinateur."""

from __future__ import annotations

import pathlib

import GPUtil
import psutil


def get_ram_usage() -> str:
  """Fonction pour récupérer l'utilisation de la RAM en Go."""
  mem = psutil.virtual_memory()
  memory_used_gb = mem.used / (1024**3)  # Conversion de bytes en gigaoctets

  return f"Mémoire utilisée : {round(memory_used_gb, 2)} Go"


def get_swap_usage() -> str:
  """Utilisation du swap.

  Returns:
      str: Utilisation du swap en GO
  """
  mem = psutil.swap_memory()
  swap_used_gb = mem.used / (1024**3)  # Conversion de bytes en gigaoctets

  return f"Swap utilisé : {round(swap_used_gb, 2)} Go"


def get_cpu_usage() -> str:
  """Utilisation du CPU.

  Returns:
      _type_: _description_
  """
  cpu = psutil.cpu_percent(interval=1)
  return f"CPU utilisé : {cpu}%"  # Pourcentage d'utilisation du CPU


def get_free_disk_space(path: str = "/") -> str:
  """Calcul de l'espace disque libre.

  Args:
      path (str, optional): Point de montage. Defaults to "/".
  """
  if not pathlib.Path(path).exists():
    raise FileNotFoundError(f"{path}")
  disk_usage = psutil.disk_usage(path)
  return f"Espace disque libre au point de montage {path}: {round(disk_usage.free / (1024 ** 3),2)} GB"  # conversison de bytes en gigaoctets


def get_gpu_usage() -> list[tuple[GPUtil.GPU, float]]:
  """Utilisation des GPU Nvidia.

  Returns:
      list[tuple[GPUtil.GPU, float]]: Objet GPU + pourcentage d'utilisation
  """
  rc = []
  gpus = GPUtil.getGPUs()
  for _, gpu in enumerate(gpus):
    print(f"  Nom: {gpu.name}")
    # conversion de mb en gb
    print(f"  Mémoire utilisée: {gpu.memoryUsed} / 1000 GB")
    # pourcentage d'utilisation gpu
    print(f"  Utilisation GPU: {gpu.load * 100}%")
    print("\n")
    rc.append((gpu, gpu.load * 100))  # pourcentage d'utilisation gpu
  return rc


def liste_processus() -> list:
  """Récupérer la liste des processus.

  Returns:
      list: Liste des noms des processus
  """
  processus = psutil.process_iter()
  return [process.name() for process in processus]
