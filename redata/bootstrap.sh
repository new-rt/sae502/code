#!/bin/bash
set -eux -o errexit
OSVER=$( (lsb_release -ds || cat /etc/*release || uname -om) 2>/dev/null | head -n1)
function elevate_priv {
    if [ "$EUID" -ne 0 ]; then
        sudo "$@"
    else
        "$@"
    fi
}

# Install dependencies

function install {
    echo installing "$1"
    case $OSVER in
    *Debian* | *Ubuntu*)
        apt_install "$@"
        ;;
    *CentOS* | *RedHat* | *Amazon*)
        yum_install "$@"
        ;;
    *SUSE*)
        zypper_install "$@"
        ;;
    *Arch*)
        pacman_install "$@"
        ;;
    *BSD*)
        pkg_install "$@"
        ;;
    *)
        echo "unknown OS $OSVER"
        exit 1
        ;;
    esac
}

function apt_install {
    elevate_priv apt-get install -y "$@"
}

function yum_install {
    elevate_priv yum install -y "$@"
}

function zypper_install {
    elevate_priv zypper install -y "$@"
}

function pacman_install {
    elevate_priv pacman -S --noconfirm "$@"
}

function pkg_install {
    elevate_priv pkg install -y "$@"
}

function pip_install {
    elevate_priv pip install "$@"
}

install python3 python3-pip git

# Install redata
cd /tmp
git clone https://gitlab.com/new-rt/sae502/code

cd code || exit 1
pip3 install -r requirements.txt
