"""Récupération des logs d'un programme ou du systeme.

Returns:
    _type_: _description_
"""

from __future__ import annotations

from typing import TYPE_CHECKING
import json
import tempfile
import shlex
import subprocess

if TYPE_CHECKING:
  from redata import connection


class Log:
  def __init__(self, connection: connection.Backend, raw: bool = False) -> None:
    self.raw = raw
    self.connection = connection

  def recuperer_log(self: "Log", fichier: str) -> str:
    """Récupérer le contenu d'un fichier log.

    Args:
        fichier (str): Nom du fichier log

    Returns:
        str: Contenu du fichier log
    """
    try:
      with tempfile.NamedTemporaryFile() as file:
        self.connection.pull_file(fichier, file.name)
        return file.read().decode("utf-8")
    except FileNotFoundError:
      return f"Le fichier journal {fichier} est introuvable."
    except PermissionError as e:
      print(e)
      return f"Vous n'avez pas les permissions pour lire le fichier {fichier}."

  def recuperer_journalctl_unite(self: "Log", nom_programme: str) -> dict | str:
    """Récupérer le contenu d'une unité systemd avec journalctl.

    Args:
        nom_programme (str): Nom de l'unité a récupérer

    Returns:
        dict: Journal au format JSON
    """
    try:
      # Échapper le nom de l'unité pour éviter les injections de commandes
      nom_echappe = shlex.quote(nom_programme)
      args = [
        "/usr/bin/journalctl",
        "-q",
        "--no-pager",
        "-u",
        nom_echappe,
        "-o",
        "json",
      ]

      # Exécuter la commande journalctl avec le nom échappé
      log: str = self.connection.run(args)
      if log == "":
        args.extend(["--user"])
        user_log = self.connection.run(args)
        if user_log == "":
          return f"L'unité {nom_programme} n'existe pas ou n'a pas de journal."
        log = user_log
      if self.raw:
        return log
      return Log.parse_systemd_json(log)
    except subprocess.CalledProcessError as e:
      return f"Erreur : {e}"

  def recuperer_journalctl(self: "Log") -> str | dict:
    """Récupérer le contenu du journal système avec journalctl.

    Returns:
        str: Journal en format JSON
    """
    try:
      # Exécuter la commande journalctl
      process = subprocess.Popen(
        ["/usr/bin/journalctl", "-q", "--no-pager", "-o", "json"],  # noqa: S603
        stdout=subprocess.PIPE,
      )
      output, _ = process.communicate()
      if self.raw:
        return output.decode("utf-8")
      return json.loads(output.decode("utf-8"))
    except subprocess.CalledProcessError as e:
      return f"Erreur : {e}"

  @staticmethod
  def parse_systemd_json(journal: str) -> dict:
    """Parser le journal systemd.

    Args:
        journal (str): Journal systemd au format JSON

    Returns:
        dict: Journal parsé
    """
    messages = journal.split("\n")
    messages[0] = "[" + messages[0]
    messages[-1] = messages[-2] + "]"
    return json.loads(",".join(messages))
