#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from __future__ import annotations

import shlex

import fabric
from loguru import logger


class Backend:
  """Classe de base pour les connexions distantes."""

  def __init__(self):
    pass

  def __connect(self):
    raise NotImplementedError

  def __disconnect(self):
    raise NotImplementedError

  def run(self, command):
    raise NotImplementedError

  def push_file(self, local_path, remote_path):
    raise NotImplementedError

  def pull_file(self, remote_path, local_path):
    raise NotImplementedError


class SSHBackend(Backend):
  """Backend pour la connexion SSH.

  Args:
    Remote (class): Classe de base héritée
  """

  def __init__(self, host, user, port=22) -> None:
    super().__init__()
    self.host = host
    self.user = user
    self.port = port
    self.con: fabric.Connection = fabric.Connection(host, user=user, port=port)

  @logger.catch(reraise=True)
  def run(self: "SSHBackend", command: str | list) -> str:
    """Exécution d'une commande sur la connexion SSH.

    Args:
      command (str): Commande à exécuter

    Returns:
      str: Résultat de la commande
    """
    if isinstance(command, str):
      return self.con.run(command).stdout
    return self.con.run(shlex.join(command)).stdout

  @logger.catch(reraise=True)
  def push_file(self: "SSHBackend", local_path: str, remote_path: str) -> None:
    """Copie d'un fichier local vers le système distant.

    Args:
      local_path (str): Chemin local du fichier
      remote_path (str): Chemin distant du fichier
    """
    self.con.put(local_path, remote_path)

  @logger.catch(reraise=True)
  def pull_file(self: "SSHBackend", remote_path: str, local_path: str) -> None:
    """Copie d'un fichier distant depuis le système distant.

    Args:
      remote_path (str): Chemin distant du fichier
      local_path (str): Chemin local du fichier
    """
    self.con.get(remote_path, local_path)

  def __del__(self: "SSHBackend") -> None:
    """Fermeture de la connexion à la suppression de l'objet."""
    self.con.close()
