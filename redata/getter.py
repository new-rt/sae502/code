#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from redata import parse_config
from redata import connection
from redata import getlog
from redata import getdata


def getlogs() -> dict:
  config: dict = parse_config.get_config_dict()
  rc = {}
  for host in config["ssh"]:
    rc[host["host"]] = {}
    ssh_backend = connection.SSHBackend(host["host"], host["user"], port=host["port"])
    logging = getlog.Log(connection=ssh_backend)
    rc[host["host"]]["files"] = {}
    rc[host["host"]]["systemd"] = {}
    for file in config["logs"]:
      if file["hosts"] == host["host"] or file["hosts"] == "all":
        rc[host["host"]]["files"][file["path"]] = logging.recuperer_log(
          file["path"]
        ).split("\n")
    for program in config["systemd"]:
      if program["hosts"] == host["host"] or program["hosts"] == "all":
        unit_log = logging.recuperer_journalctl_unite(program["name"])
        # str means error
        if isinstance(unit_log, str):
          raise ValueError(unit_log)
        # Systemd messages can sometimes be binary blobs, so we filter them out
        messages = [
          x["MESSAGE"] for x in unit_log if not isinstance(x["MESSAGE"], list)
        ]
        rc[host["host"]]["systemd"][program["name"]] = messages
  return rc


def getsysinfo():
  config: dict = parse_config.get_config_dict()
  for host in config["ssh"]:
    ssh_backend = connection.SSHBackend(host["host"], host["user"], port=host["port"])

    ssh_backend.push_file("redata/bootstrap.sh", "/tmp/bootstrap.sh")

    for info in config["sysinfo"]:
      if info["hosts"] == host["host"] or info["hosts"] == "all":
        pass


def get_alldata():
  config: dict = parse_config.get_config_dict()
  for host in config["ssh"]:
    print(host)
    ssh_backend = connection.SSHBackend(host["host"], host["user"], port=host["port"])
    sysinfo = getdata.Data()
    logging = getlog.Log(connection=ssh_backend)
    getlogs()
