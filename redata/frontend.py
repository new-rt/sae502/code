#!/usr/bin/env python3
"""Application web pour consulter les données."""

import flask
import waitress

from redata import getter, parse_config

app = flask.Flask(__name__)


@app.route("/logs")
def logs() -> str:
  """Page des logs."""
  logs = getter.getlogs()
  return flask.render_template("logs.html.j2", logs=logs)


@app.route("/")
def index() -> str:
  """Page d'accueil."""
  return flask.render_template("index.html.j2")


if __name__ == "__main__":
  app.run(debug=True, use_debugger=False, use_reloader=False, passthrough_errors=True)
  config = parse_config.get_config_dict()
  if config["web"]["bind"] == "":
    waitress.serve(app, port=config["web"]["port"])
  else:
    waitress.serve(app, host=config["web"]["bind"], port=config["web"]["port"])
  # waitress.serve(app, port=5000)
