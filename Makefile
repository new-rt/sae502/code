CTN=`which podman >/dev/null 2>&1 && echo podman || echo docker`
IMAGENAME="redata"
VERSION="latest"

.PHONY: build

build:
	$(CTN) build -t $(IMAGENAME):$(VERSION) .

clean:
	$(CTN) rmi -f $(IMAGENAME):$(VERSION)

run:
	$(CTN) run --rm=true --detach=false --name=$(IMAGENAME) $(IMAGENAME):$(VERSION)
